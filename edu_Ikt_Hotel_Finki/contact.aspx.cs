﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Net;

namespace edu_Ikt_Hotel_Finki
{
    public partial class Contact : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_Submit_Contact_Click(object sender, EventArgs e)
        {

            try
            {

                if(txtYourName.Text != "" && txtYourEmail.Text != "" && txtYourSubject.Text != "" && txtYourMessage.Text != ""){

                    using (System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage())
                    {
                        message.To.Add("hotelfinki@gmail.com");
                        message.Subject = txtYourSubject.Text;
                        message.From = new System.Net.Mail.MailAddress(txtYourEmail.Text);
                        message.IsBodyHtml = true;
                        message.Body = "\n" + txtYourMessage.Text +"\n" + "Send form: "+txtYourEmail.Text +"\n";
                        System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                        smtp.Host = "smtp.gmail.com";
                        smtp.Port = 587;//25
                        smtp.EnableSsl = true;
                        smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new System.Net.NetworkCredential("hotelfinki@gmail.com", "hotelfinki!");
                        smtp.Send(message);

                    }

                  //Display some feedback to the user to let them know it was sent
                   myAlertWarning.Style.Add("display", "none");
                   myAlert.Style.Add("display", "normal");
                  //Clear the form
                  txtYourName.Text = "";
                  txtYourMessage.Text = "";
                  txtYourSubject.Text = "";
                  txtYourEmail.Text = "";
              }
                else{
                 myAlertWarning.Style.Add("display", "normal");

               }
            }
            catch
            {
                //If the message failed at some point, let the user know
                myAlertWarning.Style.Add("display", "normal");
            }
    }
        }
    }
