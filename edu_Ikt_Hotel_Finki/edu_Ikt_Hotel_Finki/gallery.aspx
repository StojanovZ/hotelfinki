﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="gallery.aspx.cs" Inherits="edu_Ikt_Hotel_Finki.Gallery" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="server">
    Gallery
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    
<div class="hs-gallery-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="hs-room-filter" id="hs-room-filter">
                    <ul id="hs-room-filter-menu" class="list-inline list-unstyled">
                        <li class="active"><a href="#" data-filter="*" class="selected">All</a></li>
                        <li><a href="#" data-filter=".restaurant">Restaurant</a></li>
                        <li><a href="#" data-filter=".business">Business</a></li>
                        <li><a href="#" data-filter=".rooms">Rooms</a></li>
                        <li><a href="#" data-filter=".swimming-pool">Swimming Pool</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row" id="hs-rooms-list">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-sort business">
                <div class="hs-room-wrapper">
                    <div class="hs-room-image">
                        <a runat="server" href="~/assets/img/biz-01.jpg" data-lightbox="gallery">
                            <img src="assets/img/biz-01.jpg" class="img-responsive" alt="Room 1" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-sort restaurant">
                <div class="hs-room-wrapper">
                    <div class="hs-room-image">
                        <a href="assets/img/restaurant-01.jpg" data-lightbox="gallery">
                            <img src="assets/img/restaurant-01.jpg" class="img-responsive" alt="Room 1" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-sort swimming-pool">
                <div class="hs-room-wrapper">
                    <div class="hs-room-image">
                        <a href="assets/img/swim-01.jpg" data-lightbox="gallery">
                            <img src="assets/img/swim-01.jpg" class="img-responsive" alt="Room 1" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-sort rooms">
                <div class="hs-room-wrapper">
                    <div class="hs-room-image">
                        <a href="assets/img/rooms.jpg" data-lightbox="gallery">
                            <img src="assets/img/rooms.jpg" class="img-responsive" alt="Room 1" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-sort swimming-pool">
                <div class="hs-room-wrapper">
                    <div class="hs-room-image">
                        <a href="assets/img/swim-02.jpg" data-lightbox="gallery">
                            <img src="assets/img/swim-02.jpg" class="img-responsive" alt="Room 1" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-sort business">
                <div class="hs-room-wrapper">
                    <div class="hs-room-image">
                        <a href="assets/img/biz-02.jpg" data-lightbox="gallery">
                            <img src="assets/img/biz-02.jpg" class="img-responsive" alt="Room 1" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-sort restaurant">
                <div class="hs-room-wrapper">
                    <div class="hs-room-image">
                        <a href="assets/img/restaurant-02.jpg" data-lightbox="gallery">
                            <img src="assets/img/restaurant-02.jpg" class="img-responsive" alt="Room 1" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</asp:Content>
