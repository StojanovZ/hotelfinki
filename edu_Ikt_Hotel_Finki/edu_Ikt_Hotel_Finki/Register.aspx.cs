﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace edu_Ikt_Hotel_Finki
{

    public static class MessageBox
    {
        public static void Show(this Page Page, String Message)
        {
            Page.ClientScript.RegisterStartupScript(
               Page.GetType(),
               "MessageBox",
               "<script language='javascript'>alert('" + Message + "');</script>"

            );
        }
    }

    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string GetConnectionString()
        {
                  //treba da se zapise imeto na konekciskiot string od web.config file-ot
           return System.Configuration.ConfigurationManager.ConnectionStrings
                 ["daypilot"].ConnectionString;
            
        }


       private void execution(string txtname, string txtsurname, string txtmail, string txtusername, string txtpassword)
        {
          
           
           //In above line we declaring different variables same as backend
            SqlConnection conn = new SqlConnection(GetConnectionString());
            
            
           // string sql = "INSERT INTO [Users] (FirstName, SecondName, Mail, Username, Password) VALUES "
           // + " (@FirstName, @SecondName, @Mail, @Username, @Password)";
            string sql = "INSERT INTO [Users] (FirstName, SecondName, Mail, Username, Password, Rool) VALUES (@FirstName, @SecondName, @Mail, @Username, @Password, @Rool)";
            //In above lines we are just storing the sql commands which 
            //will insert value in dbo.Users named table, 
            //using variable named sql.
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                //In above lines we are opening the connection to work and
                //also storing connection name and sql command in cmd variable
                //which has 'SqlCommand' type.
                SqlParameter[] pram = new SqlParameter[6];
                //In above lines we are defining 7 sql parameters will be use
                //In below lines we will not disscuss about id column

               // pram[0] = new SqlParameter("@UserId", SqlDbType.Int, 10);
                pram[0] = new SqlParameter("@FirstName", SqlDbType.VarChar, 10);
                pram[1] = new SqlParameter("@SecondName", SqlDbType.VarChar, 10);
                pram[2] = new SqlParameter("@Mail", SqlDbType.VarChar, 10);
                pram[3] = new SqlParameter("@Username", SqlDbType.Char, 10);
                pram[4] = new SqlParameter("@Password", SqlDbType.VarChar, 10);
                pram[5] = new SqlParameter("@Rool", SqlDbType.Int, 10);

                //Now we set-uped all fiels in database in above lines
                //Now we will set-up form fields
                //pram[0].Value = random.Next(1,50);
                pram[0].Value = txtname;
                pram[1].Value = txtsurname;
                pram[2].Value = txtmail;
                pram[3].Value = txtusername;
                pram[4].Value = txtpassword;
                pram[5].Value = 0;

                //Now create loop to insert
                for (int i = 0; i < pram.Length; i++)
                {
                    cmd.Parameters.Add(pram[i]);
                }
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
            }
            catch (System.Data.SqlClient.SqlException ex_msg)
            {
                
                string msg = "Error occured while inserting";
                msg += ex_msg.Message;
                throw new Exception(msg);
            }
            finally
            {
              
                conn.Close();
            }
                  
        }

       bool IsValidEmail(string email)
       {
           try
           {
               var addr = new System.Net.Mail.MailAddress(email);
                return true;
           }
           catch
           {
               return false;
           }
       }

       bool IsPasswordEqual(string pass1, string pass2)
       {
           if (pass1.Length != pass2.Length)
           {
               return false;
           }
           else
           {
               char[] niza1 = pass1.ToArray();
               char[] niza2 = pass2.ToArray();

               for (int i = 0; i < pass1.Length; i++)
               {
                   if (niza1[i] != niza2[i])
                   {
                       return false;
                   }
               }
           }
           return true;

       }
        
        protected void btn_Submit_Click(object sender, EventArgs e)
        {

            if (txtname.Text == "" || txtusername.Text == "" || txtsurname.Text == "" || txtpassword.Text == "" || txtmail.Text == "")
            {
                myAlertWarning.Style.Add("display", "normal");
            }

            else  if (IsValidEmail(txtmail.Text) == false)
            {
                myAlertEmailWarning.Style.Add("display", "normal");
            }
            else if (IsPasswordEqual(txtpassword.Text, txtRepeatPassword.Text)==false)
            {
                myAlertEqualPassword.Style.Add("display", "normal");

            }
            else
            {

                execution(txtname.Text, txtsurname.Text,  txtmail.Text, txtusername.Text, txtpassword.Text);

                txtname.Text = "";
                txtsurname.Text = "";
                txtmail.Text = "";
                txtusername.Text = "";
                txtpassword.Text = "";
                myAlertWarning.Style.Add("display", "none");
                myAlertEmailWarning.Style.Add("display", "none");
                myAlertEqualPassword.Style.Add("display", "none");

                myAlert.Style.Add("display", "normal");

            }
            
     }

    }
}