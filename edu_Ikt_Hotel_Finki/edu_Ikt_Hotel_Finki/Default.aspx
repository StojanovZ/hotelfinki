﻿    <%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="edu_Ikt_Hotel_Finki.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="server">
    Hotel FINKI
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img class="first-slide" runat="server" src="~/assets/img/carousel/header-bg.jpg" alt="First slide">
                <div class="container">
                    <div class="carousel-caption">
                        <%--<h1 style="color: white;">Сакате опуштена и пријатна атмосфера?</h1>
                        <p style="color: white;">Одлучете се за престој на местото каде што сте третирани како вистински гостин. Опуштена атмосфера и релаксиран амбиент се само дел од услугите кои ние ги нудиме.</p>
                        <p><a class="btn btn-lg btn-primary" runat="server" href="~/rooms.aspx">Резервирај</a></p>--%>
                    </div>
                </div>
            </div>
            <div class="item">
                <img class="second-slide" runat="server" src="~/assets/img/carousel/reception.jpg" alt="Second slide">
                <div class="container">
                    <div class="carousel-caption">
                    </div>
                </div>
            </div>
            <div class="item">
                <img class="third-slide" runat="server" src="~/assets/img/carousel/room-car.jpg" alt="Third slide">
                <div class="container">
                    <div class="carousel-caption">
                    </div>
                </div>
            </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- /.carousel -->

    <div class="hs-reservation-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 res-z-index">
                    <form class="form-inline reservation-horizontal clearfix" role="form" method="post" action="#" name="reservationform" id="reservationform">
                        <div class="row">
                            <%--<div class="col-sm-3">
                            <div class="form-group">
                                <label for="email" accesskey="E">E-mail</label>
                                <input name="email" type="text" id="email" value="" class="form-control" placeholder="Please enter your E-mail">
                            </div>
                        </div>--%>
                            <div class="col-sm-3" style="width: 20%">
                                <div class="form-group">
                                    <label for="room">Room Type</label>
                                    <select class="form-control" name="room" id="room">
                                        <%-- <option selected="selected" disabled="disabled">Select a room</option>--%>
                                        <option value="Double Bedroom">Double Bedroom</option>
                                        <option value="King Size Bedroom">King Size Bedroom</option>
                                        <option value="Single Room">Single Room</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3" style="width: 20%">
                                <div class="form-group">
                                    <label for="checkin">Check-in</label>
                                    <i class="glyphicon glyphicon-calendar infield"></i>
                                    <input name="checkin" type="text" id="checkin" value="" class="form-control" placeholder="Check-in">
                                </div>
                            </div>
                            <div class="col-sm-3" style="width: 20%">
                                <div class="form-group">
                                    <label for="checkout">Check-out</label>
                                    <i class="glyphicon glyphicon-calendar infield"></i>
                                    <input name="checkout" type="text" id="checkout" value="" class="form-control" placeholder="Check-out">
                                </div>
                            </div>
                            <div class="col-sm-3" style="width: 20%">
                                <div class="form-group">
                                    <div class="guests-select">
                                        <label>Number of rooms</label>
                                        <select name="number" id="number" class="form-control">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                    </div>
                                    <%--<div class="form-group children">
                                            <label for="children">Children</label>
                                            <select name="children" id="children" class="form-control">
                                                <option value="0">0 Child(ren)</option>
                                                <option value="1">1 Child(ren)</option>
                                                <option value="2">2 Child(ren)</option>
                                                <option value="3">3 Child(ren)</option>
                                            </select>
                                        </div>--%>
                                    <%--                 <button type="button" class="hs-btn-sm hs-btn-red button-save">Save</button>--%>
                                </div>
                            </div>
                            <div class="col-sm-3" style="width: 20%">
                                <div class="form-group">
                                    <button type="submit" class="hs-btn-sm hs-btn-blue pull-right temp" style="position: relative; top: 22px;">Book Now</button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="hs-about-the-hotel">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3 class="lined-heading"><span>За ХОТЕЛ ИКТ</span></h3>
            </div>
        </div>
        <div class="hs-about-text">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <p>Луксузниот „Хотел ИКТ” се наоѓа на идеална локација во близината на центарот на Скопје.
                        Сите соби се уредени со . Сите соби имаат LCD TV, клима уред и бесплатен интернет. Поединечни соби имаат и тераси.
                    „Хотел ИКТ” покрај останатото, нуди и бесплатен приватен паркинг за кој однапред треба да резервирате.</p>
                </div>
            </div>
        </div>
        <div class="hs-about-icons">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="hs-icon-wrapper">
                        <span class="glyphicon glyphicon-glass"></span>
                        <h4>Бар</h4>
                    </div>
                    <div class="hs-icon-content">
                        <p>Опуштете се во мирниот амбиент на нашиот бар во кој се служи голем број на топли и ладни пијалоци, коктели и вина.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="hs-icon-wrapper">
                        <span class="glyphicon glyphicon-map-marker"></span>
                        <h4>Локација</h4>
                    </div>
                    <div class="hs-icon-content">
                        <p>Идеална локација од која за неколку минути можете да стигнете до најголемите атракции во градот.</p>
                    </div>
                </div>
                <!-- glyphicon glyphicon-cutlery -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="hs-icon-wrapper">
                        <span class="glyphicon glyphicon-tint"></span>
                        <h4>Базени</h4>
                    </div>
                    <div class="hs-icon-content">
                        <p>Доколку сакате да се опуштите после напорниот ден, затворените и отворените базени се сокогаш достапни за вас!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="hs-featured-rooms">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3 class="lined-heading"><span>Our Favorite Rooms</span></h3>
            </div>
        </div>
        <div class="hs-rooms-list">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="hs-room-wrapper">
                        <div class="hs-room-image">
                            <img src="assets/img/room-01.jpg" class="img-responsive" alt="Room 1" />
                        </div>
                        <div class="hs-room-info">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-title">
                                    <h5>Business Class</h5>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-price text-right">58 $</div>
                            </div>
                        </div>
                        <div class="hs-room-book">
                            <a href="room-details.html" class="hs-btn-full hs-btn-red">
                                Book now
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="hs-room-wrapper">
                        <div class="hs-room-image">
                            <img src="assets/img/room-02.jpg" class="img-responsive" alt="Room 2" />
                        </div>
                        <div class="hs-room-info">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-title">
                                    <h5>Green View</h5>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-price text-right">58 $</div>
                            </div>
                        </div>
                        <div class="hs-room-book">
                            <a href="room-details.html" class="hs-btn-full hs-btn-red">
                                Book now
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="hs-room-wrapper">
                        <div class="hs-room-image">
                            <img src="assets/img/room-03.jpg" class="img-responsive" alt="Room 3" />
                        </div>
                        <div class="hs-room-info">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-title">
                                    <h5>Family Room</h5>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-price text-right">58 $</div>
                            </div>
                        </div>
                        <div class="hs-room-book">
                            <a href="room-details.html" class="hs-btn-full hs-btn-red">
                                Book now
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</asp:Content>
