/*
 * Isotope Initialization to be used with the portfolio grid 3/4 and full width
 * */
"use strict";
jQuery(document).ready(function($){
    var $container = jQuery('#hs-rooms-list'); //The ID for the list with all the blog posts

    $container.imagesLoaded( function() {
        $container.isotope({ //Isotope options, 'item' matches the class in the PHP
            itemSelector : '.hs-sort',
            layoutMode : 'masonry',
            isFitWidth: true
        });
        $container.isotope();
    });


    //Add the class selected to the item that is clicked, and remove from the others
    var $optionSets = jQuery('#hs-room-filter-menu'),
        $optionLinks = $optionSets.find('a');

    $optionLinks.click(function(){
        var $this = $(this);
        // don't proceed if already selected
        if ( $this.hasClass('selected') ) {
            return false;
        }
        var $optionSet = $this.parents('#hs-room-filter-menu');
        $optionSets.find('.selected').removeClass('selected').closest('li').removeClass('active');
        $this.addClass('selected').closest('li').addClass('active');

        //When an item is clicked, sort the items.
        var selector = $(this).attr('data-filter');
        $container.isotope({ filter: selector });

        return false;
    });
});