﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace edu_Ikt_Hotel_Finki
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string GetConnectionString()
        {
            //treba da se zapise imeto na konekciskiot string od web.config file-ot
            return System.Configuration.ConfigurationManager.ConnectionStrings
                  ["daypilot"].ConnectionString;

        }

        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(GetConnectionString());

            string sql = "Select * from Users where Password=@Password and Username=@Username";

            try
            {
                con.Open();
                SqlCommand com = new SqlCommand(sql, con);
                com.Parameters.AddWithValue("@Username", this.txtusername.Text);
                com.Parameters.AddWithValue("@Password", this.txtpassword.Text);
                SqlDataReader dr = com.ExecuteReader();
                while (dr.Read())
                    {
                         if (dr.HasRows == true)
                          {
                          Response.Redirect("Default.aspx");
                          }
                    }
               if (dr.HasRows == false)
                {
                    myAlertWarning.Style.Add("display", "normal");
               }
            }
            catch (System.Data.SqlClient.SqlException ex_msg)
            {

                string msg = "Error occured while inserting";
                msg += ex_msg.Message;
                throw new Exception(msg);
            }
            finally
            {

                con.Close();
            }
           
            
        }
    }
}