﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="rooms.aspx.cs" Inherits="edu_Ikt_Hotel_Finki.Rooms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="server">
    Rooms
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">

    <div class="hs-rooms-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="hs-room-filter" id="hs-room-filter">
                        <ul id="hs-room-filter-menu" class="list-inline list-unstyled">
                            <li class="active"><a href="#" data-filter="*" class="selected">All</a></li>
                            <li><a href="#" data-filter=".apartment">Apartment</a></li>
                            <li><a href="#" data-filter=".single-room">Single Room</a></li>
                            <li><a href="#" data-filter=".double-room">Double Room</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row" id="hs-rooms-list">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-sort apartment">
                    <div class="hs-room-wrapper">
                        <div class="hs-room-image">
                            <img src="assets/img/room-01.jpg" class="img-responsive" alt="Room 1" />
                        </div>
                        <div class="hs-room-info">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-title">
                                    <h5>Business Class</h5>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-price text-right">58 $</div>
                            </div>
                        </div>
                        <div class="hs-room-book">
                            <a href="room-details.aspx" class="hs-btn-full hs-btn-red">Details
                        </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-sort single-room">
                    <div class="hs-room-wrapper">
                        <div class="hs-room-image">
                            <img src="assets/img/room-02.jpg" class="img-responsive" alt="Room 2" />
                        </div>
                        <div class="hs-room-info">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-title">
                                    <h5>Green View</h5>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-price text-right">58 $</div>
                            </div>
                        </div>
                        <div class="hs-room-book">
                            <a href="room-details.aspx" class="hs-btn-full hs-btn-red">Details
                        </a>
                        </div>
                    </div>
                </div>
                <%-- <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-sort double-room">
                <div class="hs-room-wrapper">
                    <div class="hs-room-image">
                        <img src="assets/img/room-03.jpg" class="img-responsive" alt="Room 3" />
                    </div>
                    <div class="hs-room-info">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-title">
                                <h5>Family Room</h5>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-price text-right">58 $</div>
                        </div>
                    </div>
                    <div class="hs-room-book">
                        <a href="room-details.aspx" class="hs-btn-full hs-btn-red">
                            Details
                        </a>
                    </div>
                </div>
            </div>--%>
                <%--<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-sort apartment">
                <div class="hs-room-wrapper">
                    <div class="hs-room-image">
                        <img src="assets/img/room-01.jpg" class="img-responsive" alt="Room 1" />
                    </div>
                    <div class="hs-room-info">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-title">
                                <h5>Business Class</h5>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-price text-right">58 $</div>
                        </div>
                    </div>
                    <div class="hs-room-book">
                        <a href="room-details.aspx" class="hs-btn-full hs-btn-red">
                            Details
                        </a>
                    </div>
                </div>
            </div>--%>
                <%--<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-sort single-room">
                <div class="hs-room-wrapper">
                    <div class="hs-room-image">
                        <img src="assets/img/room-02.jpg" class="img-responsive" alt="Room 2" />
                    </div>
                    <div class="hs-room-info">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-title">
                                <h5>Green View</h5>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-price text-right">58 $</div>
                        </div>
                    </div>
                    <div class="hs-room-book">
                        <a href="room-details.aspx" class="hs-btn-full hs-btn-red">
                            Details
                        </a>
                    </div>
                </div>
            </div>--%>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-sort double-room">
                    <div class="hs-room-wrapper">
                        <div class="hs-room-image">
                            <img src="assets/img/room-03.jpg" class="img-responsive" alt="Room 3" />
                        </div>
                        <div class="hs-room-info">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-title">
                                    <h5>Family Room</h5>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-price text-right">58 $</div>
                            </div>
                        </div>
                        <div class="hs-room-book">
                            <a href="room-details.aspx" class="hs-btn-full hs-btn-red">Details
                        </a>
                        </div>
                    </div>
                </div>
                <%--<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-sort apartment">
                <div class="hs-room-wrapper">
                    <div class="hs-room-image">
                        <img src="assets/img/room-01.jpg" class="img-responsive" alt="Room 1" />
                    </div>
                    <div class="hs-room-info">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-title">
                                <h5>Business Class</h5>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-price text-right">58 $</div>
                        </div>
                    </div>
                    <div class="hs-room-book">
                        <a href="room-details.aspx" class="hs-btn-full hs-btn-red">
                            Details
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-sort single-room">
                <div class="hs-room-wrapper">
                    <div class="hs-room-image">
                        <img src="assets/img/room-02.jpg" class="img-responsive" alt="Room 2" />
                    </div>
                    <div class="hs-room-info">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-title">
                                <h5>Green View</h5>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-price text-right">58 $</div>
                        </div>
                    </div>
                    <div class="hs-room-book">
                        <a href="room-details.aspx" class="hs-btn-full hs-btn-red">
                            Details
                        </a>
                    </div>
                </div>
            </div>--%>
         <%--       <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-sort double-room">
                    <div class="hs-room-wrapper">
                        <div class="hs-room-image">
                            <img src="assets/img/room-03.jpg" class="img-responsive" alt="Room 3" />
                        </div>
                        <div class="hs-room-info">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-title">
                                    <h5>Family Room</h5>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-6 col-xs-6 hs-col hs-room-price text-right">58 $</div>
                            </div>
                        </div>
                        <div class="hs-room-book">
                            <a href="room-details.aspx" class="hs-btn-full hs-btn-red">Details
                        </a>
                        </div>
                    </div>
                </div>--%>
            </div>
        </div>
    </div>

</asp:Content>
