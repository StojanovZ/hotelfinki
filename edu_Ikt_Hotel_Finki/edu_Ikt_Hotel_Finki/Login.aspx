﻿<%@ Page EnableEventValidation="false" Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="edu_Ikt_Hotel_Finki.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="server">
Login
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    
<div class="hs-login-page">
    <div class="container">
        <div class="row">

            <div id="myAlert" runat="server" class="alert alert-success" style="display:none;">             
                   <strong>Thank you!</strong> You are successfully registered!! 
             </div>


            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 hs-login-form">

               <div id="myAlertWarning" runat="server" class="alert alert-warning" style="display:none;">
                 <a href="#" class="close" data-dismiss="alert" >&times;</a>
                 <strong>Warning!</strong> Wrong username or password!
               </div>

                <div class="lined-heading">
                    <span>Login</span>
                </div>

                <form class="login-form" action="" method="post">
                    <div class="form-group">
                        <label>Username:</label>
                        <br />
&nbsp;<asp:TextBox ID="txtusername" runat="server" Height="33px" Width="270px"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Password:</label>
                        <br />
&nbsp;<asp:TextBox TextMode="password" ID="txtpassword" runat="server" Height="33px" Width="270px"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        &nbsp;
                        <asp:LinkButton ID="btn_Submit" runat="server" OnClick="btn_Submit_Click" Text="Submit" >
                            <span class="hs-btn-sm hs-btn-blue " ><b>Submit</b></span> 
                        </asp:LinkButton>
                       
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</asp:Content>
