﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace edu_Ikt_Hotel_Finki
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string GetConnectionString()
        {
            //treba da se zapise imeto na konekciskiot string od web.config file-ot
            return System.Configuration.ConfigurationManager.ConnectionStrings
                  ["daypilot"].ConnectionString;

        }

        protected void btn_Submit_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(GetConnectionString());

            string sql = "Select Count(*) from Users where Password=@Password and Username=@Username";
                con.Open();
                SqlCommand com = new SqlCommand(sql, con);
                com.Parameters.AddWithValue("@Username", txtusername.Text);
                com.Parameters.AddWithValue("@Password", txtpassword.Text);

                SqlDataAdapter da = new SqlDataAdapter(com);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    int type = Int32.Parse(dt.Rows[0]["Role"].ToString());
                    if (type == 0)
                    {
                        Session["USERID"] = txtusername.Text;
                        Response.Redirect("~/Admin/Default.aspx");
                    }
                    else
                    {
                        Response.Redirect("Default.aspx");
                    }
                }
        }
    }
}