﻿<%@ Page Title="Contact Us" EnableEventValidation="false"  Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="contact.aspx.cs" Inherits="edu_Ikt_Hotel_Finki.Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="server">
    Contact us  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">

    <div class="hs-contact-page">
        


        <div class="container">
                <div id="myAlert" runat="server" class="alert alert-success" style="display:none;">             
                   <strong>Thank you!</strong> Your message was sent! 
                 </div>

                <div id="myAlertWarning" runat="server" class="alert alert-warning" style="display:none;">
                 <a href="#" class="close" data-dismiss="alert" >&times;</a>
                 <strong>Warning!</strong> Your message failed to send, please try again.
               </div>


            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-contact-info">


                    <div class="lined-heading">
                        <span>Address</span>
                    </div>
                    <div class="hs-address-info">
                        <p>Rudjer Boskovic 16</p>
                        <p>Skopje, 1000, R.Macedonia </p>
                        <p class="glyphicon glyphicon-phone" style="visibility: visible"; ></p>
                        <p><span>Phone Number:</span> &nbsp; +389 70 223 305</p>
                        <p class="glyphicon glyphicon-envelope" style="visibility: visible"; ></p>
                        <p><span>E-mail:</span> hotelfinki@gmail.com</p>
                        <br /> <br />
                    </div>
                    <div class="gmap-area">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-contact-info">
                            <div class="gmap">
                                <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2964.8211855714376!2d21.409549000000016!3d42.00411300000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x33d56647e5b87264!2sFaculty+of+Computer+Science+and+Engineering!5e0!3m2!1sen!2smk!4v1430913430944" width="400" height="300" frameborder="0" style="border: 0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 hs-contact-form">
                    <div class="lined-heading">
                        <span>Send a message</span>
                    </div>
                    <div>
                        <form name="" action="" method="POST">
                            <p>
                                <label><span class="required">*</span> Your Name</label>
                                <br>
                                <asp:TextBox ID="txtYourName" runat="server" Height="37px" class="form-control" aria-required="true" aria-invalid="false"  ></asp:TextBox>
                            </p>
                            <p>
                                <label>
                                    <span class="required">*</span> E-mail</label>
                                <br>
                                 <asp:TextBox ID="txtYourEmail" runat="server" Height="37px" class="form-control" aria-required="true" aria-invalid="false"  ></asp:TextBox>

                            </p>
                            <p>
                                <label>
                                    <span class="required">*</span> Subject
                           
                                </label>
                                <br>
                                 <asp:TextBox ID="txtYourSubject" runat="server" Height="37px" class="form-control" aria-required="true" aria-invalid="false"  ></asp:TextBox>
                            </p>
                            <p>
                                <label>
                                    <span class="required">*</span> Your message
                           
                                </label>
                                <br>
                                <asp:TextBox ID="txtYourMessage" TextMode="multiline" runat="server" Columns="40" Rows="10" class="form-control" aria-invalid="false"  ></asp:TextBox>
                            </p>
                      <p class="form-group">
                        &nbsp;
                        <asp:LinkButton ID="btn_Submit_Contact" runat="server" OnClick="btn_Submit_Contact_Click" Text="Submit" >
                            <span class="hs-btn-sm hs-btn-blue " ><b>Submit</b></span> 
                        </asp:LinkButton>
                       
                        </p>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
