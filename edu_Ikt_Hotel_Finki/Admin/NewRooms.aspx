<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewRooms.aspx.cs" Inherits="New" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Insert New Room</title>
    <link href='../css/main.css' type="text/css" rel="stylesheet" /> 
</head>
<body class="dialog">
    <form id="form1" runat="server">
    <div>
        <table border="0" cellspacing="4" cellpadding="0">
            <tr>
                <td align="right"></td>
                <td>
                    <div class="header">Insert New Room</div>
                </td>
            </tr>
            <tr>
                <td align="right">Number(name) for room:</td>
                <td><asp:TextBox ID="tb_roomName" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="right">Size:</td>
                <td>
                    <asp:DropDownList ID="ddlSize" runat="server">
                        <asp:ListItem Text="1 bed" Value="1" /> 
                        <asp:ListItem Text="2 beds" Value="2" /> 
                        <asp:ListItem Text="3 beds" Value="3" /> 
                        <asp:ListItem Text="4 beds" Value="4" /> 
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right">Status:</td>
                <td>
                    <asp:DropDownList ID="DropDownListStatus" runat="server">
                        <asp:ListItem Text="Clean" Value="Clean" /> 
                        <asp:ListItem Text="Dirty" Value="Dirty" /> 
                        <asp:ListItem Text="Cleanup" Value="Cleanup" /> 
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="right"></td>
                <td>
                    <asp:Button ID="ButtonOK" runat="server" OnClick="ButtonOK_Click" Text="OK" />
                    <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" OnClick="ButtonCancel_Click" />
                </td>
            </tr>
        </table>
        
        </div>
    </form>
</body>
</html>
