using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;

public partial class New : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //numberOfRoom.Text = Int32.Parse(Request.QueryString["numberOfRoom"]);
            //ova ne trebat(trite linii podolu)
            //tb_numberOfRoom.Text = Request.QueryString["numberOfRoom"].ToString();
            //tb_size.Text = Request.QueryString["size"].ToString();
            //DropDownListStatus.SelectedIndex = Int32.Parse(Request.QueryString["status"]);

            //TextBoxName.Focus();

            //DropDownList1.DataSource = dbGetResources();
            //DropDownList1.DataTextField = "RoomName";
            //DropDownList1.DataValueField = "RoomId";
            //DropDownList1.SelectedValue = Request.QueryString["r"];
            //DropDownList1.DataBind();
        }
    }
    protected void ButtonOK_Click(object sender, EventArgs e)
    {
        string roomName = tb_roomName.Text;
        int size = Int32.Parse(ddlSize.SelectedValue);
        string status = DropDownListStatus.SelectedValue;

        dbInsertRoom(roomName, size, status);
        Modal.Close(this, "OK");
    }

    private DataTable dbGetResources()
    {
        SqlDataAdapter da = new SqlDataAdapter("SELECT [RoomId], [RoomName] FROM [Room]", ConfigurationManager.ConnectionStrings["daypilot"].ConnectionString);
        DataTable dt = new DataTable();
        da.Fill(dt);

        return dt;
    }

    private void dbInsertRoom(string roomName, int size, string status)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["daypilot"].ConnectionString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("INSERT INTO [Room] (RoomName, RoomStatus, RoomSize) VALUES(@RoomName, @RoomStatus, @RoomSize)", con);
            //cmd.Parameters.AddWithValue("id", id);
            cmd.Parameters.AddWithValue("RoomName", roomName);
            cmd.Parameters.AddWithValue("RoomStatus", status);
            cmd.Parameters.AddWithValue("RoomSize", size);
            cmd.ExecuteNonQuery();
        }
    }

    protected void ButtonCancel_Click(object sender, EventArgs e)
    {
        Modal.Close(this);
    }
}
