"use strict";

jQuery( function() {

    /* Responsive Menu */
    var toggleMenuBtn = jQuery('.hs-toggle-menu-btn');
    var navMenuList = jQuery('.hs-nav-list');
    toggleMenuBtn.on('click', function (e) {
        e.preventDefault();
        navMenuList.toggleClass('hs-collapse');
    });

    /* Guest Select */
    var guestsblock = jQuery(".guests");
    var guestsselect = jQuery(".guests-select");
    var save = jQuery(".button-save");

    guestsblock.hide();
    guestsselect.click(function() {
        guestsblock.show();
    });

    save.click(function() {
        guestsblock.fadeOut(120);
    });

    var opt1;
    var opt2;
    var total;

    jQuery('.adults select, .children select').change(function() {
        opt1 = jQuery('.adults').find('option:selected');
        opt2 = jQuery('.children').find('option:selected');
        total = +opt1.val() + +opt2.val();
        jQuery(".guests-select .total").html(total);
    });

});