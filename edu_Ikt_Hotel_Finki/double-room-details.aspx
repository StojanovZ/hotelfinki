﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="double-room-details.aspx.cs" Inherits="edu_Ikt_Hotel_Finki.room_details" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="server">
    Room Details
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    
<div class="hs-room-details">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="lined-heading">
                    <span>Double Room Details View</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 hs-room-photo">
                <img src="assets/img/room-big-03.jpg" class="img-responsive" alt="Room 1" />
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hs-room-info">
                <ul class="list-unstyled">
                    <li>Double Bed</li>
                    <li>60 square meter</li>
                    <li>Free Internet</li>
                    <li>Beach view</li>
                    <li>2 person</li>
                    <li>Flat Screen TV</li>
                    <li>Breakfast Included</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hs-book-room">
                <form class="form-inline reservation-horizontal clearfix" role="form" method="post" action="#" name="reservationform" id="reservationform">
                    <div class="row">
                       <%-- <div class="col-sm-3">
                            <div class="form-group">
                                <label for="email" accesskey="E">E-mail</label>
                                <input name="email" type="text" id="email" value="" class="form-control" placeholder="Please enter your E-mail">
                            </div>
                        </div>--%>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="room">Room Type</label>
                                <select class="form-control" name="room" id="room">
                                   <%-- <option selected="selected" disabled="disabled">Select a room</option>--%>
                                    <option value="Double Bedroom">Double Bedroom</option>
                                    <option value="King Size Bedroom">King Size Bedroom</option>
                                    <option value="Single Room">Single Room</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="checkin">Check-in</label>
                                <i class="glyphicon glyphicon-calendar infield"></i>
                                <input name="checkin" type="text" id="checkin" value="" class="form-control" placeholder="Check-in">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="checkout">Check-out</label>
                                <i class="glyphicon glyphicon-calendar infield"></i>
                                <input name="checkout" type="text" id="checkout" value="" class="form-control" placeholder="Check-out">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <div class="guests-select">
                                    <label>Number of rooms</label>
                                            <select name="number" id="number" class="form-control">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                            </select>
                                        </div>
                                        <%--<div class="form-group children">
                                            <label for="children">Children</label>
                                            <select name="children" id="children" class="form-control">
                                                <option value="0">0 Child(ren)</option>
                                                <option value="1">1 Child(ren)</option>
                                                <option value="2">2 Child(ren)</option>
                                                <option value="3">3 Child(ren)</option>
                                            </select>
                                        </div>--%>
                       <%--                 <button type="button" class="hs-btn-sm hs-btn-red button-save">Save</button>--%>
                                    
                            </div>
                       </div>
                        <div class="col-sm-12">
                            <button type="submit" class="hs-btn-sm hs-btn-blue pull-right">Book Now</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
</asp:Content>
