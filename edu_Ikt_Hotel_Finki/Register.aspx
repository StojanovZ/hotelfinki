﻿<%@ Page EnableEventValidation="false" Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="edu_Ikt_Hotel_Finki.Register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="server">
    Register
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    


<div class="hs-register-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 hs-register-form">

                 <div id="myAlert" runat="server" class="alert alert-success" style="display:none;">             
                   <strong>Thank you!</strong> You are successfully registered!! 
                 </div>

                <div id="myAlertWarning" runat="server" class="alert alert-warning" style="display:none;">
                 <a href="#" class="close" data-dismiss="alert" >&times;</a>
                 <strong>Warning!</strong> There was a problem with your input field!!
               </div>

                 <div id="myAlertEmailWarning" runat="server" class="alert alert-warning" style="display:none;">
                 <a href="#" class="close" data-dismiss="alert" >&times;</a>
                 <strong>Warning!</strong> Invalid email address!!
               </div>

               <div id="myAlertEqualPassword" runat="server" class="alert alert-warning" style="display:none;">
                 <a href="#" class="close" data-dismiss="alert" >&times;</a>
                 <strong>Warning!</strong> Your password and confirmation password do not match.
               </div>

                <div class="lined-heading">
                    <span>Register</span>
                </div>

                <form class="login-form" action="" method="post">
                    <div class="form-group">
                        <label>Name:</label>&nbsp;
                        <br />
                        <asp:TextBox ID="txtname" runat="server" Height="33px" Width="341px"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Surname:</label>
                        <br />
&nbsp;<asp:TextBox ID="txtsurname" runat="server" Height="33px" Width="341px"></asp:TextBox>
                    </div>

                     <div class="form-group">
                        <label>Mail:</label>
                         <br />
&nbsp;<asp:TextBox ID="txtmail" runat="server" Height="33px" Width="341px"></asp:TextBox>
                    </div>


                    <div class="form-group">
                        <label>Username:</label>
                        <br />
&nbsp;<asp:TextBox ID="txtusername" runat="server" Height="33px" Width="341px"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Password:</label>
                        <br />
&nbsp;<asp:TextBox TextMode="password" ID="txtpassword" runat="server" Height="33px" Width="341px"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Repeat psassword:</label>
                        <br />
&nbsp;<asp:TextBox TextMode="password" ID="txtRepeatPassword" runat="server" Height="33px" Width="341px"></asp:TextBox>
                    </div>
                   
                    
                    <div class="form-group">
                        <asp:LinkButton ID="btn_Submit" runat="server" OnClick="btn_Submit_Click" Text="Submit" Width="136px" >
                        <span class="hs-btn-sm hs-btn-blue" ><b>Submit</b></span> 
                        </asp:LinkButton>
                    </div>

                </form>
                
            </div>
        </div>
    </div>
</div>

</asp:Content>
